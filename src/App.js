import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './App.css';
import AppNavBar from './components/AppNavBar';
import Checkout from './pages/Checkout';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import Register from './pages/Register';
import SpecificProduct from './pages/SpecificProduct';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  /*
  Since reloading the app resets our user state's properties to null, we need to re-retrieve the id and isAdmin values from our API

  To do so, we run a useEffect hook with a fetch request then re-set user state's properties
  */

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser}}>
      <Router>
        <>
          <AppNavBar/>
          <Container>
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/products" component={Products}/>
                <Route exact path="/products/:productId" component={SpecificProduct}/>
                <Route exact path="/checkout" component={Checkout}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/logout" component={Logout}/>
              </Switch>
          </Container>
        </>
      </Router>
    </UserProvider>
  );
}

export default App;
