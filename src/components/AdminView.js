import { useState, useEffect } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';

export default function AdminView(props) {

	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [courseId, setCourseId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [showAdd, setShowAdd] = useState(false)
	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	const openEdit = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			setCourseId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
		setShowEdit(true)
	}

	const closeEdit = () => {
		setCourseId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
	}

	const editProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				alert("Product successfully updated")
				closeEdit()
				fetchData()
			} else {
				alert("Something went wrong")
			}
		})
	}

	const archiveToggle = (courseId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				let bool
				isActive ? bool = "disabled" : bool = "enabled"
				alert(`Product successfully ${bool}`)
				fetchData()
			} else {
				alert("Something went wrong")
			}
		})
	}

	const openAdd = () => {
		setCourseId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowAdd(true)
	}

	const closeAdd = () => {
		setCourseId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowAdd(false)
	}
	
	const addProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				alert("Product successfully added")
				closeAdd()
				fetchData()
			} else {
				alert("Something went wrong")
			}
		})
	}

	const showUserOrders = () => {
		
	}

	useEffect(() => {
		const products = productsProp.map(product => {
			return(
				<tr key={product.id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
						{
							product.isActive ? <span>Available</span> : <span>Unavailable</span>
						}
					</td>
					<td>
						<Button className="adminBtn" variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive ? <Button className="adminBtn" variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button> : <Button className="adminBtn" variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})
		setProductsArr(products)
	}, [productsProp])

	return(
		<>
			<h2 className="m-3 text-center">Admin Dashboard</h2>
			<div className="text-center">
				<Button className="m-1 adminBtn" variant="success" onClick={() => openAdd()}>Add Product</Button>
				<Button className="m-1 adminBtn" variant="primary">Show User Orders</Button>
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productsArr}
				</tbody>
			</Table>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button className="adminBtn" variant="secondary" onClick={e => closeEdit()}>Close</Button>
						<Button className="adminBtn" variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button className="adminBtn" variant="secondary" onClick={e => closeAdd()}>Close</Button>
						<Button className="adminBtn" variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}