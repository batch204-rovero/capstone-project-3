import { useContext } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="dark" variant="dark">
			<Container>
				<Link className="navbar-brand" to="/">The Zuitt Shop</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Link className="nav-link" to="/products">Products</Link>
						<Link className="nav-link" to="/checkout">Checkout</Link>
						{(user.id !== null) ?
							<Link className="nav-link" to="/logout">Log out</Link>
							:
							<>
								<Link className="nav-link" to="/register">Register</Link>
								<Link className="nav-link" to="/login">Log in</Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}