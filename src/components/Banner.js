import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({dataProp}) {
	const { title, content, label } = dataProp

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn btn-primary" to="/products">{label}</Link>
			</Col>
		</Row>
	)
}