import { Card, Col, Row } from 'react-bootstrap';

export default function Highlights() {
	return(
		<>
			<h2 className="text-center">Web Development Package</h2>
			<p className="text-center">Learn the fundamentals of web development</p>

			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title className="text-center">
								<h2>HTML</h2>
							</Card.Title>
							<Card.Text>
								Learn how to structure websites using HTML. HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title className="text-center">
								<h2>CSS</h2>
							</Card.Title>
							<Card.Text>
								Learn how to design your own websites using CSS. CSS (Cascading Style Sheets) is a style sheet language used for describing the presentation of a document written in a markup language such as HTML or XML.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title className="text-center">
								<h2>Java Script</h2>
							</Card.Title>
							<Card.Text>
								Provide functionalities for your website using JS. JS (JavaScript) is a dynamic programming language that's used for web development, in web applications, for game development, and lots more
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</>
	)
}