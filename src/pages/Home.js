import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
		title: "The Zuitt Shop",
		content: "Products for everyone, everywhere",
		label: "Buy Products"
	}

	return(
		<>
			<div>
				<Banner dataProp={data}/>
				<Highlights />
			</div>
		</>
	)
}