import { useState, useEffect, useContext } from 'react';
import { Button, Card, Form} from 'react-bootstrap';
import UserContext from '../UserContext';
import { Link, Redirect } from 'react-router-dom';

export default function Login(props) {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState("");

	const { user, setUser } = useContext(UserContext);

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	const retreiveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	function authenticateUser(e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (typeof data.access !== 'undefined') {
				localStorage.setItem("token", data.access)
				retreiveUserDetails(data.access)
				alert("User logged in")
				props.history.push("/products");
			} else {
				alert("Log in failed, please try again")
				setEmail("")
				setPassword("")
			}
		})
	}

	return(
		(user.id !== null) ?
		<Redirect to="/" />
		:
		<div className="m-3">
			<h2 className="text-center">Log in</h2>
			<div>
				<Card>
					<Card.Body>
						<Form onSubmit={e => authenticateUser(e)}>
							<Form.Group controlId="userEmail">
								<Form.Label>Email Address</Form.Label>
								<Form.Control
									type="email"
									placeholder="Enter email"
									value={email}
									onChange={e => setEmail(e.target.value)}
									required
								/>
								<Form.Text className="text-muted">
									We'll never share your email with anyone else.
								</Form.Text>
							</Form.Group>

							<Form.Group controlId="password">
								<Form.Label>Password</Form.Label>
								<Form.Control
									type="password"
									placeholder="Enter password"
									value={password}
									onChange={e => setPassword(e.target.value)}
									required
								/>
							</Form.Group>

							{isActive ?
								<Button className="w-100 mt-3" variant="primary" type="submit" id="submitBtn">
								Submit
								</Button>
								:
								<Button className="w-100 mt-3" variant="primary" id="submitBtn" disabled>
								Submit
								</Button>				
							}
						</Form>
					</Card.Body>
				</Card>
			</div>
			<p className="m-2 text-center">Don't have an account yet? <Link to="/register" style={{ textDecoration: 'none' }}>Click here</Link> to register</p>
		</div>
	)

}