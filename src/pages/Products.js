import { useState, useEffect, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';

export default function Products() {

	const [productsData, setProductsData] = useState([]);

	const { user } = useContext(UserContext);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			setProductsData(data);
		})
	};

	useEffect(() => {
		fetchData();
	}, []);

	const products = productsData.map(product => {
		return(
			<ProductCard productProp={product} key={product.id} />
		)
	})

	return(
		(user.isAdmin) ?
		<AdminView productsProp={productsData} fetchData={fetchData}/>
		:
		<>
			<h1 className="mt-3">Products</h1>
			{products}
		</>
	)
}