import { useState, useEffect, useContext } from 'react';
import { Button, Card, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Link, Redirect } from 'react-router-dom';

export default function Register(props){

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	const { user } = useContext(UserContext);

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password1, password2])

	function registerUser(e){
		e.preventDefault() //prevent default form behavior, so that the form does not submit
		
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Duplicate email exists")
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						alert("Successfully registered")
						props.history.push("/login");
					} else {
						alert("Something went wrong")
					}
				})
			}
		})
	}

	return(
		(user.id !== null) ?
		<Redirect to="/" />
		:
		<div>
			<h2>Register</h2>
			<div>
				<Card>
					<Card.Body>
						<Form onSubmit={e => registerUser(e)}>
							<Form.Group controlId="userEmail">
								<Form.Label>Email Address</Form.Label>
								<Form.Control
									type="email"
									placeholder="Enter email"
									value={email}
									onChange={e => setEmail(e.target.value)}
									required
								/>
								<Form.Text className="text-muted">
									We'll never share your email with anyone else.
								</Form.Text>
							</Form.Group>

							<Form.Group controlId="password1">
								<Form.Label>Password</Form.Label>
								<Form.Control
									type="password"
									placeholder="Enter password"
									value={password1}
									onChange={e => setPassword1(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="password2">
								<Form.Label>Verify Password</Form.Label>
								<Form.Control
									type="password"
									placeholder="Verify password"
									value={password2}
									onChange={e => setPassword2(e.target.value)}
									required
								/>
							</Form.Group>

							{isActive ?
								<Button className=" w-100 mt-3" variant="primary" type="submit" id="submitBtn">
								Submit
								</Button>
								:
								<Button className="w-100 mt-3" variant="primary" id="submitBtn" disabled>
								Submit
								</Button>				
							}
						</Form>
					</Card.Body>
				</Card>
			</div>
			<p className="mt-2 text-center">Already have an account? <Link to="/login" style={{ textDecoration: 'none' }}>Clich here</Link> to log in</p>
		</div>
	)
}