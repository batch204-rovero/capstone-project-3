import { useState, useEffect } from 'react';
import { Card, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function SpecificProduct({match}) {

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const productId = match.params.productId;

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	return(
		<div className="m-3">
			<h2 className="text-center">Add to Cart</h2>
			<div>
				<Card>
					<Card.Body>
						<Form>
							<Form.Group>
								<Form.Label>{name}</Form.Label>
							</Form.Group>
							<Form.Group>
								<Form.Label>{description}</Form.Label>
							</Form.Group>
							<Form.Group>
								<Form.Label>PhP {price}</Form.Label>
							</Form.Group>

							<Link className="btn btn-primary" to="/checkout">Purchase</Link>
						</Form>
					</Card.Body>
				</Card>
			</div>
		</div>
	)
}
